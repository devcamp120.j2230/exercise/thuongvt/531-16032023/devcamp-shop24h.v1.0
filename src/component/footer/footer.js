
import ComponentFooter1 from "./ComponentFooter1"
import ComponentFooter2 from "./ComponentFooter2"
import ComponentFooter3 from "./ComponentFooter3"
import ComponentSocialFooter from "./ComponentSocialFooter"

function Footer() {
    return (
        <>
                <div style={{ paddingBottom: "50px", paddingTop: "50px", backgroundColor: "lightgrey" }}>
                    <div className="row">
                        <div className="col-3">
                            <ComponentFooter1></ComponentFooter1>
                        </div>
                        <div className="col-3">
                            <ComponentFooter2></ComponentFooter2>
                        </div>
                        <div className="col-3">
                            <ComponentFooter3></ComponentFooter3>
                        </div>
                        <div className="col-3">
                            <ComponentSocialFooter></ComponentSocialFooter>
                        </div>
                    </div>

                </div>
        </>

    )
}

export default Footer