import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import imgCarousel1 from "../../assetment/image/JBL Endurance Sprint-products-8.jpg";
import imgCarousel2 from "../../assetment/image/JBL JR 310T pink-products-3.jpg";
import imgCarousel3 from "../../assetment/image/JBL UA Project Rock-products-7 .jpg";


function ContentCarousel() {

    const settings = {
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        
    }
    return (
        <div className="container">
            <Slider {...settings}>
                <div>
                    <div className="container-fluid">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text" >
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">JBL Quantum 100</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>Ipsum dolor</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn btn-dark mt-4 shop-now">SHOP NOW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <img className="carousel-image" src={imgCarousel1} alt="carousel-1" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="container-fluid">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text">
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">JBL T510BT</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>Ipsum dolor</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn btn-dark mt-4 shop-now" style={{ borderRadius: "0" }}>SHOP NOW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <img className="carousel-image" src={imgCarousel2} alt="carousel-2" style={{width:"500px"}}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="container-fluid">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="row">
                                        <div className="col-sm-6 carousel-text" >
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold">JBL Quantum 100</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p className="fw-bold " style={{ fontSize: "50px" }}>Ipsum dolor</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <p style={{ lineHeight: "1.5", opacity: "0.8" }}>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <button className="btn btn-dark mt-4 shop-now">SHOP NOW</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <img className="carousel-image" src={imgCarousel3} alt="carousel-1" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </Slider>
        </div>
    )
}

export default ContentCarousel